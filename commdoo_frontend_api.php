<?php

define('COMMDOO_FRONTEND_API_URL', 'https://frontend.payment-transaction.net/payment.aspx');

class commdoo_frontend_api {

  public function get_settings_form($settings = array()) {
    $form = array();
    $form['clientid'] = array(
      '#type' => 'textfield',
      '#title' => t('Client ID'),
      '#required' => TRUE,
      '#default_value' => isset($settings['clientid']) ? $settings['clientid'] : '',
    );

    $form['sharedsecret'] = array(
      '#type' => 'textfield',
      '#title' => t('Shared secret'),
      '#required' => TRUE,
      '#default_value' => isset($settings['sharedsecret']) ? $settings['sharedsecret'] : '',
    );

    return $form;
  }

  public function get_redirect_form($form, $order, $payment_method, $user, $language_entity, $paymentkey = 'all') {
    $clientid     = $payment_method['settings']['clientid'];
    $sharedsecret = $payment_method['settings']['sharedsecret'];

    $failurl    = url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE));
    $successurl = url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE));

    $wrapper  = entity_metadata_wrapper('commerce_order', $order);
    $currency = $wrapper->commerce_order_total->currency_code->value();
    $amount   = $wrapper->commerce_order_total->amount->value();

    $firstname             = '';
    $lastname              = '';
    $country               = '';
    $city                  = '';
    $postalcode            = '';
    $street_and_housnumber = '';

    $billing_address_array = array();
    drupal_alter('commerce_commdoo_billing_address_array', $billing_address_array, $order);
    if (!empty($billing_address_array)) {
      if (!empty($billing_address_array['FirstName'])) {
        $firstname = $billing_address_array['FirstName'];
      } else if (!empty($billing_address_array['First_Name'])) {
        $firstname = $billing_address_array['First_Name'];
      }

      if (!empty($billing_address_array['LastName'])) {
        $lastname = $billing_address_array['LastName'];
      } else if (!empty($billing_address_array['Last_Name'])) {
        $lastname = $billing_address_array['Last_Name'];
      }

      if (empty($firstname) && empty($lastname)) {
        $name = '';
        if (!empty($billing_address_array['Name'])) {
          $name = $billing_address_array['Name'];
        } else if (!empty($billing_address_array['Name_Line'])) {
          $name = $billing_address_array['Name_Line'];
        }

        if (!empty($name)) {
          $name_parts = explode(' ', $name);
          $firstname = array_shift($name_parts);
          $lastname = implode(' ', $name_parts);
        }
      }

      if (!empty($billing_address_array['Country'])) {
        $country = country_load($billing_address_array['Country'])->iso3;
      }

      if (!empty($billing_address_array['Locality'])) {
        $city = $billing_address_array['Locality'];
      } else if (!empty($billing_address_array['City'])) {
        $city = $billing_address_array['City'];
      }

      if (!empty($billing_address_array['PostalCode'])) {
        $postalcode = $billing_address_array['PostalCode'];
      } else if (!empty($billing_address_array['Postal_Code'])) {
        $postalcode = $billing_address_array['Postal_Code'];
      }

      if (!empty($billing_address_array['ThoroughFare'])) {
        $street_and_housnumber = $billing_address_array['ThoroughFare'];
      } else if (!empty($billing_address_array['Street'])) {
        $street_and_housnumber = $billing_address_array['Street'];
        if (!empty($billing_address_array['HouseNumber'])) {
          $street_and_housnumber .= ' '.$billing_address_array['HouseNumber'];
        }
      }
    }

    if (!empty($wrapper->commerce_customer_billing->value()->commerce_customer_address)) {
      $address = $wrapper->commerce_customer_billing->commerce_customer_address;

      if (empty($firstname) && empty($lastname)) {
        if (!empty($address->first_name->value())) {
          $firstname = $address->first_name->value();
        }

        if (!empty($address->last_name->value())) {
          $lastname = $address->last_name->value();
        }
      }

      if (empty($firstname) && empty($lastname) && !empty($address->name_line->value())) {
        $name_parts = explode(' ', $address->name_line->value());
        $firstname = array_shift($name_parts);
        $lastname = implode(' ', $name_parts);
      }

      if (!empty($address->country->value())) {
        $country = country_load($address->country->value())->iso3;
      }

      if (!empty($address->locality->value())) {
        $city = $address->locality->value();
      }

      if (!empty($address->postal_code->value())) {
        $postalcode = $address->postal_code->value();
      }

      if (!empty($address->thoroughfare->value())) {
        $street_and_housnumber = $address->thoroughfare->value();
      }
    }

    $orderid = $order->order_number;
    $language = $this->get_language($language_entity);
    $email = $order->mail;
    $website = url('<front>', array('absolute' => TRUE));

    date_default_timezone_set('Europe/Berlin');
    $timestamp = date("dmYHis");

    $referenceid = md5($orderid.$website.$amount.$currency.$timestamp);
    $referencecustomerid = $user->uid;

    $form['#action'] = COMMDOO_FRONTEND_API_URL;

    $string_to_hash = $clientid.$paymentkey.$referenceid.$referencecustomerid.$language.
                      $amount.$currency.$firstname.$lastname.$street_and_housnumber.
                      $postalcode.$city.$country.$email.$successurl.$failurl.$timestamp.
                      $website.$sharedsecret;
    $hash = sha1($string_to_hash);

    $form['clientid'] = array('#type' => 'hidden', '#value' => $clientid);
    $form['payment'] = array('#type' => 'hidden', '#value' => $paymentkey);
    $form['referenceid'] = array('#type' => 'hidden', '#value' => $referenceid);

    if (!empty($referencecustomerid)) {
      $form['referencecustomerid'] = array('#type' => 'hidden', '#value' => $referencecustomerid);
    }

    if (!empty($language)) {
      $form['language'] = array('#type' => 'hidden', '#value' => $language);
    }

    $form['amount'] = array('#type' => 'hidden', '#value' => $amount);
    $form['currency'] = array('#type' => 'hidden', '#value' => $currency);

    if (!empty($firstname)) {
      $form['firstname'] = array('#type' => 'hidden', '#value' => $firstname);
    }

    if (!empty($lastname)) {
      $form['lastname'] = array('#type' => 'hidden', '#value' => $lastname);
    }

    if (!empty($street_and_housnumber)) {
      $form['street'] = array('#type' => 'hidden', '#value' => $street_and_housnumber);
    }

    if (!empty($postalcode)) {
      $form['postalcode'] = array('#type' => 'hidden', '#value' => $postalcode);
    }

    if (!empty($city)) {
      $form['city'] = array('#type' => 'hidden', '#value' => $city);
    }

    if (!empty($country)) {
      $form['country'] = array('#type' => 'hidden', '#value' => $country);
    }

    if (!empty($email)) {
      $form['emailaddress'] = array('#type' => 'hidden', '#value' => $email);
    }

    $form['successurl'] = array('#type' => 'hidden', '#value' => $successurl);
    $form['failurl'] = array('#type' => 'hidden', '#value' => $failurl);
    $form['timestamp'] = array('#type' => 'hidden', '#value' => $timestamp);
    if (!empty($website)) {
      $form['website'] = array('#type' => 'hidden', '#value' => $website);
    }

    $form['hash'] = array('#type' => 'hidden', '#value' => $hash);

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Proceed with payment'),
    );

    $transaction = $this->load_transaction($order->order_id, $payment_method['method_id']);
    $transaction->instance_id = $payment_method['instance_id'];
    $transaction->currency_code = $currency;
    $transaction->amount = $amount;
    $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
    $transaction->payload['referenceid'] = $referenceid;
    commerce_payment_transaction_save($transaction);

    return $form;
  }

  public function get_iframe($form, $order, $payment_method, $user, $language_entity, $paymentkey = 'all') {
    $clientid     = $payment_method['settings']['clientid'];
    $sharedsecret = $payment_method['settings']['sharedsecret'];

    $failurl    = url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE));
    $successurl = url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE));

    $wrapper  = entity_metadata_wrapper('commerce_order', $order);
    $currency = $wrapper->commerce_order_total->currency_code->value();
    $amount   = $wrapper->commerce_order_total->amount->value();

    $firstname             = '';
    $lastname              = '';
    $country               = '';
    $city                  = '';
    $postalcode            = '';
    $street_and_housnumber = '';

    $billing_address_array = array();
    drupal_alter('commerce_commdoo_billing_address_array', $billing_address_array, $order);
    if (!empty($billing_address_array)) {
      if (!empty($billing_address_array['FirstName'])) {
        $firstname = $billing_address_array['FirstName'];
      } else if (!empty($billing_address_array['First_Name'])) {
        $firstname = $billing_address_array['First_Name'];
      }

      if (!empty($billing_address_array['LastName'])) {
        $lastname = $billing_address_array['LastName'];
      } else if (!empty($billing_address_array['Last_Name'])) {
        $lastname = $billing_address_array['Last_Name'];
      }

      if (empty($firstname) && empty($lastname)) {
        $name = '';
        if (!empty($billing_address_array['Name'])) {
          $name = $billing_address_array['Name'];
        } else if (!empty($billing_address_array['Name_Line'])) {
          $name = $billing_address_array['Name_Line'];
        }

        if (!empty($name)) {
          $name_parts = explode(' ', $name);
          $firstname = array_shift($name_parts);
          $lastname = implode(' ', $name_parts);
        }
      }

      if (!empty($billing_address_array['Country'])) {
        $country = country_load($billing_address_array['Country'])->iso3;
      }

      if (!empty($billing_address_array['Locality'])) {
        $city = $billing_address_array['Locality'];
      } else if (!empty($billing_address_array['City'])) {
        $city = $billing_address_array['City'];
      }

      if (!empty($billing_address_array['PostalCode'])) {
        $postalcode = $billing_address_array['PostalCode'];
      } else if (!empty($billing_address_array['Postal_Code'])) {
        $postalcode = $billing_address_array['Postal_Code'];
      }

      if (!empty($billing_address_array['ThoroughFare'])) {
        $street_and_housnumber = $billing_address_array['ThoroughFare'];
      } else if (!empty($billing_address_array['Street'])) {
        $street_and_housnumber = $billing_address_array['Street'];
        if (!empty($billing_address_array['HouseNumber'])) {
          $street_and_housnumber .= ' '.$billing_address_array['HouseNumber'];
        }
      }
    }

    if (!empty($wrapper->commerce_customer_billing->value()->commerce_customer_address)) {
      $address = $wrapper->commerce_customer_billing->commerce_customer_address;

      if (empty($firstname) && empty($lastname)) {
        if (!empty($address->first_name->value())) {
          $firstname = $address->first_name->value();
        }

        if (!empty($address->last_name->value())) {
          $lastname = $address->last_name->value();
        }
      }

      if (empty($firstname) && empty($lastname) && !empty($address->name_line->value())) {
        $name_parts = explode(' ', $address->name_line->value());
        $firstname = array_shift($name_parts);
        $lastname = implode(' ', $name_parts);
      }

      if (!empty($address->country->value())) {
        $country = country_load($address->country->value())->iso3;
      }

      if (!empty($address->locality->value())) {
        $city = $address->locality->value();
      }

      if (!empty($address->postal_code->value())) {
        $postalcode = $address->postal_code->value();
      }

      if (!empty($address->thoroughfare->value())) {
        $street_and_housnumber = $address->thoroughfare->value();
      }
    }

    $orderid = $order->order_number;
    $language = $this->get_language($language_entity);
    $email = $order->mail;
    $website = url('<front>', array('absolute' => TRUE));

    date_default_timezone_set('Europe/Berlin');
    $timestamp = date("dmYHis");

    $referenceid = md5($orderid.$website.$amount.$currency.$timestamp);
    $referencecustomerid = $user->uid;

    $string_to_hash = $clientid.$paymentkey.$referenceid.$referencecustomerid.$language.
                      $amount.$currency.$firstname.$lastname.$street_and_housnumber.
                      $postalcode.$city.$country.$email.$successurl.$failurl.$timestamp.
                      $website.$sharedsecret;
    $hash = sha1($string_to_hash);

    $url = COMMDOO_FRONTEND_API_URL;
    $url .= '?clientid='.urlencode($clientid);
    $url .= '&payment='.urlencode($paymentkey);
    $url .= '&referenceid='.urlencode($referenceid);

    if (!empty($referencecustomerid)) {
      $url .= '&referencecustomerid='.urlencode($referencecustomerid);
    }

    if (!empty($language)) {
      $url .= '&language='.urlencode($language);
    }

    $url .= '&amount='.urlencode($amount);
    $url .= '&currency='.urlencode($currency);

    if (!empty($firstname)) {
      $url .= '&firstname='.urlencode($firstname);
    }

    if (!empty($lastname)) {
      $url .= '&lastname='.urlencode($lastname);
    }

    if (!empty($street_and_housnumber)) {
      $url .= '&street='.urlencode($street_and_housnumber);
    }

    if (!empty($postalcode)) {
      $url .= '&postalcode='.urlencode($postalcode);
    }

    if (!empty($city)) {
      $url .= '&city='.urlencode($city);
    }

    if (!empty($country)) {
      $url .= '&country='.urlencode($country);
    }

    if (!empty($email)) {
      $url .= '&emailaddress='.urlencode($email);
    }

    $url .= '&successurl='.urlencode($successurl);
    $url .= '&failurl='.urlencode($failurl);
    $url .= '&timestamp='.urlencode($timestamp);
    if (!empty($website)) {
      $url .= '&website='.urlencode($website);
    }

    $url .= '&hash='.urlencode($hash);

    $form['iframe'] = array(
      '#markup' => '<iframe src="'.$url.'" name="commdoo-test" scrolling="no" frameborder="0" width="600px" height="900px"></iframe>',
    );

    $transaction = $this->load_transaction($order->order_id, $payment_method['method_id']);
    $transaction->instance_id = $payment_method['instance_id'];
    $transaction->currency_code = $currency;
    $transaction->amount = $amount;
    $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
    $transaction->payload['referenceid'] = $referenceid;
    commerce_payment_transaction_save($transaction);

    return $form;
  }

  public function confirm($order, $payment_method) {
    $method_name = $payment_method['method_id'];
    if (!$order) {
      watchdog($method_name, 'Could not confirm payment transaction, because the order could not be loaded', array(), WATCHDOG_ERROR);
      return false;
    }

    $transaction = $this->load_transaction($order->order_id, $method_name);
    if (!$transaction) {
      watchdog($method_name, 'Could not confirm payment transaction, because the transaction could not be loaded', array(), WATCHDOG_ERROR);
      return false;
    }

    $transaction->payload[REQUEST_TIME . '-log'] = $_GET;
    commerce_payment_transaction_save($transaction);

    if (!$payment_method) {
      $payment_method = commerce_payment_method_instance_load($transaction->instance_id);
    }

    if (!$payment_method) {
      watchdog($method_name, 'Could not confirm payment transaction, because the payment method could not be loaded', array(), WATCHDOG_ERROR);
      return false;
    }

    $clientid     = $payment_method['settings']['clientid'];
    $sharedsecret = $payment_method['settings']['sharedsecret'];

    $paramclientid             = (isset($_GET['clientid'])                  ? $_GET['clientid']                  : '');
    $transactionid             = (isset($_GET['transactionid'])             ? $_GET['transactionid']             : '');
    $referenceid               = (isset($_GET['referenceid'])               ? $_GET['referenceid']               : '');
    $subscriptionid            = (isset($_GET['subscriptionid'])            ? $_GET['subscriptionid']            : '');
    $amount                    = (isset($_GET['amount'])                    ? $_GET['amount']                    : '');
    $currency                  = (isset($_GET['currency'])                  ? $_GET['currency']                  : '');
    $paymentmethod             = (isset($_GET['paymentmethod'])             ? $_GET['paymentmethod']             : '');
    $customerid                = (isset($_GET['customerid'])                ? $_GET['customerid']                : '');
    $transactionstatus         = (isset($_GET['transactionstatus'])         ? $_GET['transactionstatus']         : '');
    $transactionstatusaddition = (isset($_GET['transactionstatusaddition']) ? $_GET['transactionstatusaddition'] : '');
    $creditcardtype            = (isset($_GET['creditcardtype'])            ? $_GET['creditcardtype']            : '');
    $providertransactionid     = (isset($_GET['providertransactionid'])     ? $_GET['providertransactionid']     : '');
    $errornumber               = (isset($_GET['errornumber'])               ? $_GET['errornumber']               : '');
    $errortext                 = (isset($_GET['errortext'])                 ? $_GET['errortext']                 : '');
    $additionaldata            = (isset($_GET['additionaldata'])            ? $_GET['additionaldata']            : '');
    $timestamp                 = (isset($_GET['timestamp'])                 ? $_GET['timestamp']                 : '');
    $hash                      = (isset($_GET['hash'])                      ? $_GET['hash']                      : '');

    $transactionid             = urldecode($transactionid);
    $referenceid               = urldecode($referenceid);
    $subscriptionid            = urldecode($subscriptionid);
    $amount                    = urldecode($amount);
    $currency                  = urldecode($currency);
    $paymentmethod             = urldecode($paymentmethod);
    $customerid                = urldecode($customerid);
    $transactionstatus         = urldecode($transactionstatus);
    $transactionstatusaddition = urldecode($transactionstatusaddition);
    $creditcardtype            = urldecode($creditcardtype);
    $providertransactionid     = urldecode($providertransactionid);
    $errornumber               = urldecode($errornumber);
    $errortext                 = urldecode($errortext);
    $additionaldata            = urldecode($additionaldata);
    $timestamp                 = urldecode($timestamp);
    $hash                      = urldecode($hash);

    if ($clientid != $paramclientid) {
      watchdog($method_name, 'Could not confirm payment transaction, because the provided clientid is not the same as the configured one', array(), WATCHDOG_ERROR);
      return false;
    }

    $string_to_hash = $paramclientid.$transactionid.$referenceid.$subscriptionid.$amount.$currency.
                      $paymentmethod.$customerid.$transactionstatus.$transactionstatusaddition.
                      $creditcardtype.$providertransactionid.$errornumber.$errortext.$additionaldata.$timestamp.$sharedsecret;

    $hash = strtoupper(trim($hash));
    $calculated_hash = strtoupper(sha1($string_to_hash));

    if ($calculated_hash != $hash) {
      watchdog($method_name, 'Could not confirm payment transaction, because of an invalid hash: expected @calculated_hash but got @hash', array('@calculated_hash' => $calculated_hash, '@hash' => $hash), WATCHDOG_ERROR);

      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = t('Could not verify CommDoo Frontend API response, because of an invalid hash (maybe fraudulent manipulation)');
      commerce_payment_transaction_save($transaction);
      return false;
    }

    if (!empty($errornumber) || !empty($errortext)) {
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = t('CommDoo Frontend API reported an error: '.$errornumber.' - '.$errortext);
      commerce_payment_transaction_save($transaction);
      return false;
    }

    if (!empty($transactionstatus)) {
      $transaction->remote_status = $transactionstatus;
      $commdoo_status = $transactionstatus;
      if (!empty($transactionstatusaddition)) {
        $commdoo_status .= ' - '.$transactionstatusaddition;
      }

      $transaction->remote_status = $commdoo_status;
    }

    $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
    $transaction->remote_id = $transactionid;
    $transaction->message = t('Payment completed');
    commerce_payment_transaction_save($transaction);
    watchdog($method_name, 'Confirmed order @order_id', array('@order_id' => $order->order_id));
    return true;
  }

  public function load_transaction($order_id, $method_name) {
    $transactions = commerce_payment_transaction_load_multiple(array(), array('order_id' => $order_id, 'payment_method' => $method_name));
    if (!empty($transactions)) {
      $max_id = -1;
      $most_recent_transaction = null;

      foreach ($transactions as $transaction_id => $transaction) {
        if (isset($transaction) && $transaction_id > $max_id) {
          $most_recent_transaction = $transaction;
          $max_id = $transaction_id;
        }
      }

      if ($most_recent_transaction != null && $most_recent_transaction->status == COMMERCE_PAYMENT_STATUS_PENDING) {
        return $most_recent_transaction;
      }
    }

    return commerce_payment_transaction_new($method_name, $order_id);
  }

  private function get_language($language_entity) {
    $language_code = drupal_strtoupper($language_entity->language);
    $language_code = explode('_', $language_code);
    $language_code = $language_code[0];

    $supported_language_codes = array(
      'CS' => 'ces',
      'DA' => 'dan',
      'DE' => 'deu',
      'EN' => 'eng',
      'ES' => 'spa',
      'FI' => 'fin',
      'FR' => 'fra',
      'IT' => 'ita',
      'NL' => 'nld',
      'NB' => 'nor',
      'NN' => 'nor',
      'NO' => 'nor',
      'PL' => 'pol',
      'SV' => 'swe'
    );

    if (isset($supported_language_codes[$language_code])) {
      return $supported_language_codes[$language_code];
    }

    $language_prefix = drupal_strtoupper($language_entity->prefix);
    $language_prefix = explode('-', $language_prefix);
    $language_prefix = $language_prefix[0];

    $default_languages = array(
      'CZ' => 'ces',
      'DK' => 'dan',
      'DE' => 'deu',
      'US' => 'eng',
      'UK' => 'eng',
      'CA' => 'eng',
      'ES' => 'spa',
      'FI' => 'fin',
      'SF' => 'fin',
      'FR' => 'fra',
      'IT' => 'ita',
      'NL' => 'nld',
      'NO' => 'nor',
      'PL' => 'pol',
      'SE' => 'swe'
    );

    if (isset($default_languages[$language_prefix])) {
      return $default_languages[$language_prefix];
    }

    return 'eng';
  }
}
